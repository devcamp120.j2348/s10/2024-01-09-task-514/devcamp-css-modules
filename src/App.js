import avatar from './assets/images/avatar.jpg';
import style from './App.module.css';

function App() {
  return (
    <div className={style.dcContainer}>
      <img src={avatar} className={style.dcImage} />
      <p className={style.dcQuote}>This is one of the best developer blogs on the planet! I read it daily to improve my skills</p>
      <p><b>Tammy Stevens</b> Front End Developer</p>
    </div>
  );
}

export default App;
